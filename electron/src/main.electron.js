'use strict'

import { app, protocol, BrowserWindow, Menu } from 'electron';
import { createProtocol, installVueDevtools } from 'vue-cli-plugin-electron-builder/lib';
import contextMenu from 'electron-context-menu';
import jetpack from "fs-jetpack";

import { appWindow } from './app/windows/appWindow'
import { devMenu } from './app/menus/devMenu'
import { helpMenu } from './app/menus/helpMenu'
import { viewMenu } from './app/menus/viewMenu'
import { fileMenu } from './app/menus/fileMenu'

// detect electron
process.env.IS_ELECTRON = true;

const IS_DEV = process.env.NODE_ENV !== 'production' && !process.env.IS_TEST;
const BASE_URL = IS_DEV ? process.env.WEBPACK_DEV_SERVER_URL : 'app://./index.html';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let shell;

// Standard schemes (app://) must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
    {
        scheme: 'app',
        privileges: { standard: true, secure: true, supportFetchAPI: true }
    }
]);

function init() {
    // must be called after app.on('ready') event is fired
    if (!IS_DEV) createProtocol('app');

    // create main window
    shell = appWindow();

    // build menubar and set it as the application menu
    const app_menu_bar = [fileMenu(), viewMenu(), helpMenu({parentWindow: shell})];

    // add devMenu only if we are in development
    if (IS_DEV) app_menu_bar.push(devMenu());

    // add empty menu item to beginning of menu to fix mac issue where it renames the first item to 'electron'
    if(process.platform == 'darwin')
        app_menu_bar.unshift({});

    Menu.setApplicationMenu(Menu.buildFromTemplate(app_menu_bar));

    // electron-context-menu
    var contextMenuOptions = {
        shouldShowMenu: (event, params) => !params.isEditable
    };
    // avoid bug in production when setting showInspectElement to false (solution is to not include this option in production)
    if (IS_DEV) contextMenuOptions['showInspectElement'] = true;

    contextMenu(contextMenuOptions);

    shell.show();
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin')
        app.quit();
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (shell === null)
        init();
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    if (IS_DEV && !process.env.IS_TEST) {
        // Install Vue Devtools
        try {
            await installVueDevtools();
        } catch (e) {
            console.error('Vue Devtools failed to install:', e.toString());
        }
    }
    init();
});

// Exit cleanly on request from parent process in development mode.
if (IS_DEV) {
    if (process.platform === 'win32') {
        process.on('message', data => {
            if (data === 'graceful-exit') {
                app.quit();
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit();
        })
    }
}

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 * https://www.electron.build/auto-update ** <-- LOOK AT THIS
 */
/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
    autoUpdater.quitAndInstall();
});

app.on('ready', () => {
    if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates();
});
*/
