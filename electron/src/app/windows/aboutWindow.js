
import { BrowserWindow } from 'electron';

const IS_DEV = process.env.NODE_ENV !== 'production' && !process.env.IS_TEST;
const URL = IS_DEV ? process.env.WEBPACK_DEV_SERVER_URL+'#about' : 'app://./index.html#about';

var webPreferences = {
    nodeIntegration: true,
    webSecurity: false
};

export const aboutWindow = (args = {}) => {
    let win = new BrowserWindow({
        nodeIntegration: true,
        show: false,
        title: 'App Info',
        minWidth: 400,
        minHeight: 400,
        width: 1024,
        height: 400,
        useContentSize: true,
        icon: __static + '/favicon.ico',
        modal: true,
        parent: args.parent,
        webPreferences
    });

    win.loadURL(URL);

    win.on('closed', () => {
        win = null;
    });

    return win;
};
