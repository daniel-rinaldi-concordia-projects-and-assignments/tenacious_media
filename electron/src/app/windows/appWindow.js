
import { BrowserWindow } from 'electron';

const IS_DEV = process.env.NODE_ENV !== 'production' && !process.env.IS_TEST;
const URL = IS_DEV ? process.env.WEBPACK_DEV_SERVER_URL : 'app://./index.html';

var webPreferences = {
    nodeIntegration: true,
    webSecurity: false
};

export const appWindow = (args = {}) => {
    let win = new BrowserWindow({
        show: false,
        title: "App",
        center: true,
        minWidth: 600,
        minHeight: 568,
        width: 1024,
        height: 768,
        useContentSize: true,
        icon: __static + '/favicon.ico',
        webPreferences
    });

    win.loadURL(URL);

    win.on('closed', () => {
        win = null;
    });

    return win;
};
