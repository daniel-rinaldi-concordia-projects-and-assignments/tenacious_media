
import { app, BrowserWindow } from "electron";

export const devMenu = (args = {}) => {
    return {
        label: 'Dev',
        submenu: [
            {
                label: 'Refresh',
                accelerator: 'F5',
                click() {
                    BrowserWindow.getFocusedWindow().reload();
                }
            },
            {
                label: 'Refresh (no cache)',
                accelerator: 'CmdOrCtrl+F5',
                click() {
                    BrowserWindow.getFocusedWindow().webContents.reloadIgnoringCache();
                }
            },
            { type: 'separator' },
            {
                label: 'Toggle Devtools',
                accelerator: 'F12',
                click() {
                    BrowserWindow.getFocusedWindow().toggleDevTools();
                }
            }
        ]
    };
};
