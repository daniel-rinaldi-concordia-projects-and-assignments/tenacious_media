
import { app, BrowserWindow } from "electron";

export const fileMenu = (args = {}) => {
    return {
        label: 'File',
        submenu: [
            {
                label: 'Exit',
                accelerator: 'CmdOrCtrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    };
};
