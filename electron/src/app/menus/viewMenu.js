
import { app, BrowserWindow } from "electron";

export const viewMenu = (args = {}) => {
    return {
        label: 'View',
        submenu: [
            {
                label: 'Toggle fullscreen',
                accelerator: 'F11',
                click() {
                    BrowserWindow.getFocusedWindow().setFullScreen(!BrowserWindow.getFocusedWindow().isFullScreen());
                }
            }
        ]
    };
};
