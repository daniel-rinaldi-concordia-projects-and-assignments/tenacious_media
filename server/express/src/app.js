const express = require('express');
const cors = require('cors');
const app = express();
const path = require('path');

// set the view engine to ejs
app.set('views', 'src/views');
app.set('view engine', 'ejs');

// parse json request body
app.use(express.json());

// serve static files
app.use(express.static(path.join(__dirname + '/views/www')));
app.use('/assets', express.static(path.join(__dirname + '/assets/public')));

const CORS_ALLOWED_ORIGINS = ['http://example.com/', 'http://www.example.com/', 'http://localhost/'];
app.use(cors({
    origin: function(origin, callback) {
        if(!origin) // allow requests with no origin (like mobile apps or curl requests)
            return callback(null, true);

        var match = false;
        for(i = 0; i < CORS_ALLOWED_ORIGINS.length; i++) {
            if(new RegExp('^'+origin).test(CORS_ALLOWED_ORIGINS[i]))
                match = true;
        }

        if(!match) {
            var msg = "The CORS policy for this site does not allow access from the specified Origin: '"+origin+"'";
            return callback(new Error(msg), false);
        }

        return callback(null, true);
    }
}));

// api routes (web services)
var api_path = "/api/";
app.use(api_path+"example", require('./web_services/ws_example'));

// fallback route for requests not matched above
app.use(function(req, res, next) {
    // if request was made to API respond with json, else let the frontend router handle it
    if (req.url.match(/^\/api\//))
        res.status(404).send("API method not found.");
    else
        res.sendFile(path.join(__dirname + '/views/www/index.html'));
});


module.exports = app;
