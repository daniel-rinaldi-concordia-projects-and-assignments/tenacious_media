import Vue from 'vue'
import Router from 'vue-router'

import PrimaryRoute from './views/PrimaryRoute.vue'
import HomePage from './views/PrimaryRoute/HomePage.vue'
import MusicLibraryPage from './views/PrimaryRoute/MusicLibraryPage.vue'
import SettingsPage from './views/PrimaryRoute/SettingsPage.vue'

import Route from './views/Route.vue'
import AboutPage from './views/Route/AboutPage.vue'

import ErrorRoute from './views/ErrorRoute.vue'
import E404Page from './views/ErrorRoute/404.vue'

Vue.use(Router);

const router = new Router({
    mode: (!process.env.IS_ELECTRON && !process.env.CORDOVA_PLATFORM ? 'history' : 'hash'),
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'route-primary',
            component: PrimaryRoute,
            children: [
                {
                    path: 'home',
                    alias: '',
                    name: 'home',
                    component: HomePage,
                    meta: {
                        scrollTo: { y:0 }
                    }
                },
                {
                    path: 'music_library',
                    name: 'music library',
                    component: MusicLibraryPage,
                    meta: {
                        scrollTo: { y:0 }
                    }
                },
                {
                    path: 'settings',
                    name: 'settings',
                    component: SettingsPage,
                    meta: {
                        scrollTo: { y:0 }
                    }
                }
            ]
        },
        {
            path: '/',
            name: 'route',
            component: Route,
            children: [
                {
                    path: 'about',
                    name: 'about',
                    component: AboutPage,
                    meta: {
                        scrollTo: { y:0 }
                    }
                }
            ]
        },
        {
            path: '/',
            name: 'route-error',
            component: ErrorRoute,
            children: [
                {
                    // this should be the last child of ErrorRoute
                    path: '*',
                    name: 'e404',
                    component: E404Page,
                    meta: {
                        scrollTo: { y:0 }
                    }
                }
            ]
        }
    ],

    /* scrollBehavior:
     * - only available in html5 'history' mode
     * - defaults to no scroll behavior
     * - return false to prevent scroll
     */
    scrollBehavior: (to, from, savedPosition) => {
        if (savedPosition) {
            // savedPosition is only available for popstate navigations. (browser back/forward buttons)
            return savedPosition;
        }
        else {
            const position = {};
            // new navigation.
            // scroll to anchor by returning the selector
            if (to.hash) {
                position.selector = to.hash;
            }
            // check if any matched route config has meta that requires scrolling to position
            if (to.meta.scrollTo) {
                if(to.meta.scrollTo.x == 0 || to.meta.scrollTo.x) {
                    position.x = to.meta.scrollTo.x;
                }

                if(to.meta.scrollTo.y == 0 || to.meta.scrollTo.y) {
                    position.y = to.meta.scrollTo.y;
                }
            }

            // if the returned position is falsy or an empty object,
            // we will retain the current scroll position.
            return position;
        }
    }
});

// do stuff after each router transition
// router.afterEach((toRoute, fromRoute) => {
//
// });

export default router;
