
// prevents FOUT (Flash of Unstyled Text) for icon fonts

var css = document.createElement('style');
css.type = 'text/css';
css.id = 'iconfont-fout-fix';

var styles = '.fas, .far, .fal, .fab { opacity: 0; }';

if (css.styleSheet) css.styleSheet.cssText = styles;
else css.appendChild(document.createTextNode(styles));

document.body.appendChild(css);

window.addEventListener("load", () => {
    var el = document.getElementById('iconfont-fout-fix');
    el.parentNode.removeChild(el);
});
