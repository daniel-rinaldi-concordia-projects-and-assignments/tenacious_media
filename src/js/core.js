
const Core = {
    scanForAudioFiles: async (music_directories, callback) => {
        if(process.env.CORDOVA_PLATFORM !== undefined)
        {
            const MusicMetadata = require("music-metadata-browser");
            var nativeURLs = music_directories;

            const scanDirectory = (entry) => {
                var dirReader = entry.createReader();
                dirReader.readEntries(
                    (entries) => {
                        var regex_audio = /\.(?:wav|mp3|ogg)$/i;
                        for (var i = 0; i < entries.length; i++) {
                            if (entries[i].isDirectory === true) {
                                // Recursive -- call back into this subdirectory
                                scanDirectory(entries[i]);
                            }
                            else if (entries[i].isFile === true && entries[i].name.match(regex_audio)) {
                                entries[i].file((file) => {
                                    MusicMetadata.parseBlob(file, {native: true}).then((metadata) => {
                                        var html5_audio = new Audio(file.localURL);
                                        callback({
                                            title: metadata.common.title ? metadata.common.title : file.name,
                                            artist: metadata.common.artist ? metadata.common.artist : 'unknown',
                                            album: metadata.common.album ? metadata.common.album : 'unknown',
                                            duration: metadata.format.duration ? metadata.format.duration : 0,
                                            picture: Core.getMusicArtURI(metadata.common.picture),
                                            audio: html5_audio,
                                            visualizer: {}
                                        });
                                    }).catch((e) => {
                                        console.warn(
                                            'There was a problem getting media tags for file "' + file.name + '": ' +
                                            e.message + '\n Using fallback strategy. '
                                        );
                                        var html5_audio = new Audio(file.localURL);
                                        callback({
                                            title: file.name,
                                            artist: 'unknown',
                                            album: 'unknown',
                                            duration: 0,
                                            audio: html5_audio,
                                            picture: undefined,
                                            visualizer: {}
                                        });
                                    });
                                },
                                (err) => {
                                    console.log("Failed to get file from entry: " + err.message);
                                });
                            }
                        }
                    },
                    function (error) {
                        console.error("readEntries error: " + error.code);
                    }
                );
            }; // scanDirectory - end

            for (var i = 0; i < nativeURLs.length; i++) {
                if (nativeURLs[i] === null || nativeURLs[i] === undefined || nativeURLs[i].length === 0)
                    continue; // skip non-existent / blank paths

                window.resolveLocalFileSystemURL(nativeURLs[i], scanDirectory,
                (error) => {
                    console.log("scanDirectory error: " + error.code);
                });
            }
        } // cordova - end
        else if(process.env.IS_ELECTRON) {
            const MusicMetadata = require("music-metadata");
            const fs = require('fs');
            var directories = music_directories;

            const scanDirectory = (directory) => {
                fs.readdir(directory, (err, files) => {
                    if (!err) {
                        files.forEach(filename => {
                            fs.stat(directory+filename, (lstat_err, fileStats, lstat_args={directory, filename}) => {
                                if(lstat_err){
                                    console.error(lstat_err);
                                }
                                else {
                                    var regex_audio = /\.(?:wav|mp3|ogg)$/i;
                                    if (fileStats.isDirectory()) {
                                        // Recursive -- call back into this subdirectory
                                        scanDirectory(lstat_args.directory+lstat_args.filename+"/");
                                    }
                                    else if (fileStats.isFile() && lstat_args.filename.match(regex_audio)) {
                                        MusicMetadata.parseFile(lstat_args.directory+lstat_args.filename, {native: true}).then((metadata) => {
                                            var visualizer = {};
                                            visualizer.context = new AudioContext();
                                            visualizer.analyser = visualizer.context.createAnalyser();
                                            var html5_audio = new Audio(lstat_args.directory+lstat_args.filename);
                                            visualizer.source = visualizer.context.createMediaElementSource(html5_audio);
                                            callback({
                                                title: metadata.common.title ? metadata.common.title : lstat_args.filename,
                                                artist: metadata.common.artist ? metadata.common.artist : 'unknown',
                                                album: metadata.common.album ? metadata.common.album : 'unknown',
                                                duration: metadata.format.duration ? metadata.format.duration : 0,
                                                picture: Core.getMusicArtURI(metadata.common.picture),
                                                audio: html5_audio,
                                                visualizer
                                            });
                                        }).catch((e) => {
                                            console.warn(
                                                'There was a problem getting media tags for file "' + lstat_args.filename + '": ' +
                                                e.message + '\n Using fallback strategy... '
                                            );
                                            var visualizer = {};
                                            visualizer.context = new AudioContext();
                                            visualizer.analyser = visualizer.context.createAnalyser();
                                            var html5_audio = new Audio(lstat_args.directory+lstat_args.filename);
                                            visualizer.source = visualizer.context.createMediaElementSource(html5_audio);
                                            callback({
                                                title: lstat_args.filename,
                                                artist: 'unknown',
                                                album: 'unknown',
                                                duration: 0,
                                                audio: html5_audio,
                                                visualizer,
                                                picture: undefined
                                            });
                                        });
                                    }
                                }
                            });
                        });
                    }
                    else {
                        console.error("readdir error: " + err.message);
                    }
                });
            }; // scanDirectory - end

            for (var j = 0; j < directories.length; j++) {
                if (directories[j] === null || directories[j] === undefined || directories[j].length === 0)
                    continue; // skip non-existent / blank paths

                scanDirectory(directories[j]);
            }
        } // electron - end
    },

    getMusicArtURI(picture_data) {
        var imageURI = undefined;
        if (picture_data !== undefined && picture_data.length > 0 && picture_data[0].format !== undefined && picture_data[0].format.match(/^image/g)) {
            var base64String = "";
            for (var i = 0; i < picture_data[0].data.length; i++)
                base64String += String.fromCharCode(picture_data[0].data[i]);

            imageURI = "data:" + picture_data[0].format + ";base64," + window.btoa(base64String);
        }

        return imageURI;
    }
};

export default Core;
