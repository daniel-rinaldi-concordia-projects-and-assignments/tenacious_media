const ToolBox = {
    /**
    * Shuffles array in place.
    * @param {Array} a items An array containing the items.
    */
    shuffle: (a) => {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    },

    // toggle class on element. supports IE9+
    toggleClass: (el, className) => {
        if (el.classList)
            el.classList.toggle(className);
        else {
            var classes = el.className.split(' ');
            var existingIndex = classes.indexOf(className);

        if (existingIndex >= 0)
            classes.splice(existingIndex, 1);
        else
            classes.push(className);

            el.className = classes.join(' ');
        }
    },

    // converts a form to a json object with key-value pairs being [name]:[value] for every input within the form
    formToJson: (selector) => {
        var json = {};

        var inputs = document.querySelectorAll(
            selector + 'input:not([app-form-ignore]):not([type="button"]):not([type="submit"]):not([type="reset"]), ' +
            selector + 'textarea:not([app-form-ignore])'
        );

        for(var i = 0; i < inputs.length; i++) {
            var name = inputs[i].getAttribute("name");
            var value = "";

            // skip unchecked checkboxes
            if(inputs[i].type && inputs[i].type === 'checkbox' && !inputs[i].checked) continue;
            // skip unchecked radio inputs
            if(inputs[i].type && inputs[i].type === 'radio' && !inputs[i].checked) continue;

            value = inputs[i].value;

            if (name) json[name] = value;
        }

        return json;
    }
};

export default ToolBox;
