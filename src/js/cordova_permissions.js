
// Reference: https://github.com/dpa99c/cordova-diagnostic-plugin#usage

const Permissions = {
    name: 'permissions',

    requestExternalStorageAuthorization: function() {
        console.log("Requesting external storage authorization...");
        cordova.plugins.diagnostic.getExternalStorageAuthorizationStatus(
            function (status) {
                if (status == cordova.plugins.diagnostic.permissionStatus.GRANTED)
                    console.log("External storage use is already authorized.");
                else {
                    cordova.plugins.diagnostic.requestExternalStorageAuthorization(
                        function (result) {
                            console.log("Authorization request for external storage use was " + (result.READ_EXTERNAL_STORAGE == cordova.plugins.diagnostic.permissionStatus.GRANTED ? "granted." : "denied."));
                        },
                        function (error) {
                            console.error("Error occurred when requesting external storage authorization: " + error);
                        }
                    );
                }
            },
            function (error) {
                console.error("Error occurred when checking ExternalStorageAuthorizationStatus: " + error);
            }
        );
    }
};

export default Permissions;
