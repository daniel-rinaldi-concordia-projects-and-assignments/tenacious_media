import Vue from 'vue'
// import axios from 'axios'

import router from './router.js'
import store from './store.js'

import './js/iconfont_fout.js'

import 'typeface-roboto/index.css'
import '@fortawesome/fontawesome-free/css/all.css'
import './css/vuetify-overrides.css'
import './css/styles.css'

import 'core-js/shim'
import './plugins/vuetify'
import './js/polyfill.js'

import CordovaPermissions from './js/cordova_permissions'

import App from './App.vue'

Vue.config.productionTip = false;

function start() {
    /* eslint-disable no-new */
    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#app-mount');
}

// using cordova
if (process.env.CORDOVA_PLATFORM !== undefined) {
    // for cordova we listen for 'deviceready' event
    document.addEventListener('deviceready', function () {
        CordovaPermissions.requestExternalStorageAuthorization();
        start();
    });
}
else { // not using cordova ('deviceready' event was not fired)
    start();
}
