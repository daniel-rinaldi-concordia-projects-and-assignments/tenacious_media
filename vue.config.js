
// configuration reference
// https://cli.vuejs.org/config/#global-cli-config

const webpack = require('webpack')

module.exports = {
    // vue-cli config
    outputDir: 'vue-dist',

    // webpack config
    configureWebpack: {
        // always modify vue config webpack properties first before modifying the usual webpack properties
        // https://cli.vuejs.org/guide/webpack.html  <--- check this first
        // https://webpack.js.org/configuration/  <--- fallback option
        entry: {
            app: './src/main.vue.js'
        }
    },
    pluginOptions: {
        // electron builder config
        // https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/configuration.html#configuring-electron-builder
        electronBuilder: {
            outputDir: 'electron/dist',
            mainProcessFile: 'electron/src/main.electron.js',
            builderOptions: {
                // options placed here will be merged with default configuration and passed to electron-builder
                // https://www.electron.build/configuration/configuration
                productName: "Tenacious Media",
                appId: "com.tenaciousdan.media",
                // artifactName: ${productName}-${version}.${ext},
                // copyright: "",
                directories: {
                  output: 'electron/dist/build'
                },
                mac: {
                    icon: 'public/icons/icon.icns'
                },
                win: {
                    icon: 'public/icons/icon.ico'
                },
                linux: {
                    icon: 'public/icons'
                }
            }
        },

        // cordova config
        cordovaPath: 'cordova'
    },
    // needed for cordova (production build)
    publicPath: ''
}
