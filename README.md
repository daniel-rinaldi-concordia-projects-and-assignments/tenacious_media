# Tenacious Media

## Project setup
```
npm install
npm run cordova:prepare
```

### Compiles and hot-reloads for development
Browser:  
expressJS: `npm run express:serve`  
vue (frontend only): `npm run serve`  

Electron:  
`npm run electron:serve`

Cordova:  
android: first, using a separate terminal, start the emulator : `PATH_TO_ANDROID_SDK/emulator/emulator -avd AVD_NAME` and then `npm run cordova:serve-android`.
Alternatively, you can edit the `cordova:emulate-android` property in `package.json` and run that command instead.

iOS: `npm run cordova:serve-ios`

### Compiles, minifies and build for production
Browser:  
vue (frontend only): `npm run build`

Electron:  
`npm run electron:build`

Cordova:  
android: `npm run cordova:build-android`  
iOS: `npm run cordova:build-ios`  

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Vue CLI Configuration Reference](https://cli.vuejs.org/config/)

See [Vue CLI Webpack Configuration Reference](https://cli.vuejs.org/guide/webpack.html)

See [Webpack Configuration Reference](https://webpack.js.org/configuration/)

See [Vue CLI Electron Builder Configuration Reference](https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/configuration.html#configuring-electron-builder)

See [Electron Builder Configuration Reference](https://www.electron.build/configuration/configuration)
